# README #

### What is this repository for? ###

* This Repo is still a work in progress.  The Repo is a central storage space for mods that have been contributed to the LayerFused X Series printers.

### Mods ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

## If you would like your mod added to this repo there are some things that should be done before submitting it. ##
* Printed and fitted to an X-series printer
* Purpose of the mod should be known
* Be willing to share step files and/or Fusion files
* Pictures of installation  

### Who do I talk to? ###

* Reach out to Dano on the Discord
* https://discord.gg/rY3VkTCfGb